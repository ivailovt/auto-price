<?php

/**
 * Class Inix2Module
 */
class Inix2Module extends Module
{

    /**
     * Constructor
     *
     * @param string $name Module unique name
     * @param Context $context
     */
    public function __construct($name = null, Context $context = null)
    {

        parent::__construct($name, $context);
        $this->warning = $this->l(
            'Inixweb framework not detected.' .
            ' For further information contact the support of the marketplace where you bought our module.'
        );
    }

    /**
     * Insert module into datable
     */
    public function install()
    {
        $this->context->controller->errors[] = $this->l(
            'Inixweb framework not detected.' .
            ' For further information contact the support of the marketplace where you bought our module.'
        );
    }

    /**
     * Delete module from datable
     *
     * @return bool result
     */
    public function uninstall()
    {

    }
}
