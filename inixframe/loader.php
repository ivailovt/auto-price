<?php

if (!defined('_PS_VERSION_')) {
    exit;
}
$service_url = 'http';
$service_url .= '://';
$service_url .='service';
$service_url .='.';
$service_url .='presta';
$service_url .='-';
$service_url .='sandbox';
$service_url .='.';
$service_url .='com';

if ($env_url = @getenv('UPDATE_SERVICE_URL')) {
    $service_url = $env_url;
}

$extract = false;
if (Tools::file_exists_no_cache(dirname(__FILE__) . '/version')) {
    $packed_version = Tools::file_get_contents(dirname(__FILE__) . '/version');
    if (!Tools::file_exists_no_cache(_PS_MODULE_DIR_ . 'inixframe/InixModule.php')) {
        $extract = true;
    } elseif (Tools::file_exists_no_cache(_PS_MODULE_DIR_ . 'inixframe/version')) {
        $installed_version = Tools::file_get_contents(_PS_MODULE_DIR_ . 'inixframe/version');
        if (Tools::version_compare($packed_version, $installed_version, '>')) {
            $extract = true;
        } else {
            $extract = false;
            require_once _PS_MODULE_DIR_ . 'inixframe/InixModule.php';
        }
    }
}

if ($extract) {
    $remote_version = @Tools::file_get_contents($service_url . 'serve/inixframe/latest/version');
    $packed_version = Tools::file_get_contents(dirname(__FILE__) . '/version');
    if ((bool) $remote_version &&
        (
            (!isset($installed_version) && Tools::version_compare($packed_version, $remote_version, '<')) ||
            (isset($installed_version) && Tools::version_compare($installed_version, $remote_version, '<'))
        )
    ) {
        $inixframe = Tools::file_get_contents($service_url . 'serve/inixframe/latest');
        if ((bool) $inixframe) {
            Tools::deleteFile(dirname(__FILE__) . '/inixframe.zip');
            file_put_contents(dirname(__FILE__) . '/inixframe.zip', $inixframe);
        }
    }
}

if ($extract) {
    $res = false;
    if (class_exists('ZipArchive', false)) {
        $zip = new ZipArchive();
        $res = $zip->open(dirname(__FILE__) . '/inixframe.zip');
        if ($res) {
            $res = $zip->extractTo(_PS_MODULE_DIR_);
        }
    }
    if (!$res) {
        if (!class_exists('PclZip', false)) {
            require_once(_PS_TOOL_DIR_ . 'pclzip/pclzip.lib.php');
        }
        $zip = new PclZip(dirname(__FILE__) . '/inixframe.zip');
        $zip->extract(PCLZIP_OPT_PATH, _PS_MODULE_DIR_);
    }
}
if (Tools::file_exists_no_cache(_PS_MODULE_DIR_ . 'inixframe/InixModule.php')) {
    require_once _PS_MODULE_DIR_ . 'inixframe/InixModule.php';
}
if (!class_exists('Inix2Module')) {
    require_once dirname(__FILE__) . '/InixModule.php';
}
