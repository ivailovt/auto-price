<?php

	/**
	 * Class CustomForm
	 */
	class AutoPrices extends ObjectModel {

		public $id;
		public $name;
		public $currency;
		public $country;
		public $group;
		public $date_start;
		public $date_end;
		public $quantity;
		public $discount;
		//public $reduction_tax;
		public $mode;
		public $active;
		public $categories = array();
		public $fields = array();
		public $errors = array();


		public static $definition = array(
			'table'=> 'autoprice',
			'primary'=> 'id_autoprice',
			'multilang'=> true,
			'fields' => array(
            	'name'=> array(
					'type' => self::TYPE_STRING,
					'required' => true,
					'size' => 128,
					'lang' => true,
					'validate' => 'isGenericName'
				),
				'currency' => array(
					'type' => self::TYPE_INT,
                    'size'     => 11,
                    'validate' => 'isUnsignedInt',
            	),
            	'country' => array(
					'type' => self::TYPE_INT,
                    'size'     => 11,
                    'validate' => 'isUnsignedInt',
            	),
				'group' => array(
					'type' => self::TYPE_INT,
	                'size'     => 11,
	                'validate' => 'isUnsignedInt',
            	),
				'date_start' => array(
					'type' => self::TYPE_DATE,
					'required' => true,
					//'validate' => 'isDate'	
				),
				'date_end' => array(
					'type' => self::TYPE_DATE,
					'required' => true,
					//'validate' => 'isDateFormat'			
				),
				'quantity' => array(
					'type' => self::TYPE_INT,
	                'size'     => 11,
	                'validate' => 'isUnsignedInt',
	                'required' => true
            	),
				'discount' => array(
					'type' => self::TYPE_FLOAT,
                    'validate' => 'isFloat',
                    'required' => true
            	),
            	//'reduction_tax' => array(
					//'type' => self::TYPE_INT,
	                //'size'     => 11,
	                //'validate' => 'isUnsignedInt',
            	//),
				'mode' => array(
					'type' => self::TYPE_BOOL,
                    'validate' => 'isBool',
                    'required' => true
            	),
            	'active' => array(
            		'type' => self::TYPE_BOOL, 
            		'validate' => 'isBool',
            		'required' => false
            	)
			)
		);


		
		/**
	     * @param null $id
	     * @param null $id_lang
	     * @param null $id_shop
	     */
		public function __construct($id = null, $id_lang = null, $id_shop = null)
		{

			parent::__construct($id, $id_lang, $id_shop);

				if(Validate::isLoadedObject($this)){
					$cat = DB::getInstance()->executeS('SELECT * FROM `'._DB_PREFIX_.'autoprice_categories` WHERE id_autoprice = '.$this->id);
					foreach($cat as $c){
						$this->categories[] = $c['id_category'];
					}
				}

		}



		/**
	     * @param bool|true  $autodate
	     * @param bool|false $null_values
	     *
	     * @return bool
	     */
		public function add($autodate = true, $null_values = false)
		{

			$validate = $this->dateValidate();
			if(!empty($validate)) {
				return false;
			} else {
				$this->insertSpecificPrices();
				$ret = parent::add($autodate, $null_values);
				if($ret){
					foreach($this->categories as $c){
						DB::getInstance()->insert('autoprice_categories', array('id_autoprice' => $this->id, 'id_category' => (int)$c));
					}
				}

				return $ret;
			}

		}



		/**
	     * @param bool|false $null_values
	     *
	     * @return bool
	     */
		public function update($null_values = false)
		{

			$validate = $this->dateValidate();
			if(!empty($validate)) {
				return false;
			} else {
				$this->insertSpecificPrices();
				$ret = parent::update($null_values);
	            if($ret){         	
					DB::getInstance()->delete('autoprice_categories', 'id_autoprice = '.$this->id);
					foreach($this->categories as $c){
						DB::getInstance()->insert('autoprice_categories', array('id_autoprice' => $this->id, 'id_category' => (int)$c));
					}
				}
	
				return $ret;
			}

		}



		/**
	     * @return bool
	     */
		public function delete()
		{

			$this->deleteSpecificPrices();
			$ret = parent::delete();
			if($ret){
				DB::getInstance()->delete('autoprice_categories', 'id_autoprice = '.$this->id);
			}

			return $ret;

		}



		/**
	     * @return string
	     */
		public function dateValidate()
		{

			$id_default_lang = (int) Configuration::get('PS_LANG_DEFAULT');
			$date = false;

			if($this->discount){
				if(strlen((float)$this->discount) > 8){
					$errors[] = Tools::displayError('Field discount is not valid.');
				} elseif ((float)$this->discount <= 0 || (float)$this->discount > 100) {
		           	$errors[] = Tools::displayError('Submitted reduction value (0-100) is out-of-range');
		        }
		    }
	        if(empty($this->categories)) {
	           	$errors[] = Tools::displayError('Field categories is required.');
	        }
	        if($this->date_start && $this->date_end){
				if(!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) (0[0-9]|1[0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/", (string)(strlen($this->date_start) < 19 ? $this->date_start.':00' : $this->date_start)) && !preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) (0[0-9]|1[0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/", (string)(strlen($this->date_end) < 19 ? $this->date_end.':00' : $this->date_end))) {  				
		            $errors[] = Tools::displayError('Wrong format of the date entered.');
		   		} else {
		       		$date_start_post = new DateTime($this->date_start);
		            $date_end_post = new DateTime($this->date_end);
		   		   			
			   		if($date_start_post >= $date_end_post) {
					     $errors[] = Tools::displayError('Incorrect entry dates. Start date '.$this->date_start.' can not be less than or equal to the end date '.$this->date_end.'.');	      
					} elseif ($this->categories) {
			            $result = array();
			            $sql = 'SELECT a.`id_autoprice`, a.`date_start`, a.`date_end`, cl.`name`
			                FROM `' . _DB_PREFIX_ . 'autoprice` a
			                JOIN `' . _DB_PREFIX_ . 'autoprice_categories` ac
			                ON (a.id_autoprice  = ac.id_autoprice)
			                JOIN `' . _DB_PREFIX_ . 'category` c
			                ON (ac.id_category  = c.id_category)
			                JOIN `' . _DB_PREFIX_ . 'category_lang` cl
			                ON (c.id_category  = cl.id_category AND cl.id_lang = '.$id_default_lang.')
			                WHERE ac.id_category IN( ' . implode(",", $this->categories) . ')';
			            $result = DB::getInstance()->executeS($sql);

			            foreach($result as $res) {
			            	if(!empty($res['date_start']) && !empty($res['date_end'])) {
				                $date_start_db = new DateTime($res['date_start']);
				                $date_end_db = new DateTime($res['date_end']);

				                if($date_end_post >= $date_start_db && $date_start_post <= $date_end_db) {
				                	if($this->id == '' or $this->id != $res['id_autoprice']) {
					              		$errors[] = Tools::displayError('Entered period coincides with the rule created earlier period '.$res['date_start'].' - '.$res['date_end'].' in the '.$res['name'].' category.');				   				
										$date = true;				   				
					   				}
				                }
			            	}
			            }
			       	            
			            $result1 = array();
	 		  			$sql1 = 'SELECT `from`, `to`
	 		       			FROM `' . _DB_PREFIX_ . 'specific_price`';
	 		    		$result1 = DB::getInstance()->executeS($sql1);

			            $result2 = array();
	 		    		$sql2 = 'SELECT `date_start`, `date_end`
	 		   				FROM `' . _DB_PREFIX_ . 'autoprice`
	 		        		WHERE id_autoprice = "'.$this->id.'"';
	 		   			$result2 = DB::getInstance()->getRow($sql2);
	 		   			$table = 'specific_price';

			            foreach($result1 as $r) {
			            	if(!empty($r['from']) && !empty($r['to'])) {
				                $date_start = new DateTime($r['from']);
				                $date_end = new DateTime($r['to']);

				                if($date_end_post >= $date_start && $date_start_post <= $date_end && $date == false) {
				                	if($result2['date_start'] != $r['from'] && $result2['date_end'] != $r['to']) {
					              		$errors[] = sprintf(Tools::displayError('Entered period coincides with the rule created earlier period '.$r['from'].' - '.$r['to']. ' in the %s table.'), $table);				   				
					   				}
				                }
			            	}
			            }
				   	}
		       	}
		    }
		        
			return $errors;

		}



		/**
	     *
	     */
		public function insertSpecificPrices()
		{

			if($this->id && !Tools::getIsset('addautoprice')){
				$this->deleteSpecificPrices();
			}

     		if($this->active == 1){

	            if(isset($this->categories)){          	
	            	$reduction_type = 'percentage';
			    	$result = array();
					$query = new DbQuery();
				    $query->select('p.id_product');
				    $query->from('category_product', 'p');
				   	$query->where( 'p.id_category  IN ( '.implode(",", $this->categories).')' );
				   	$query->groupBy('p.id_product');
				    $result = DB::getInstance()->executeS($query);

					foreach($result as $res){
						$product = new SpecificPrice();
						$product->id_product = $res['id_product'];
						$product->id_shop = 0;
						$product->id_currency = $this->currency;
						$product->id_country = $this->country;
						$product->id_group = $this->group;
						$product->id_customer = 0;
						$product->price = -1.000000;
						$product->from_quantity = $this->quantity;
						$product->reduction = (float)($reduction_type == 'percentage' ? $this->discount / 100 : $this->discount);
						$product->reduction_type = $reduction_type;
						$product->from = (string)(strlen($this->date_start) < 19 ? $this->date_start.':00' : $this->date_start);
						$product->to = (string)(strlen($this->date_end) < 19 ? $this->date_end.':00' : $this->date_end);
						$product->save();
					}
				}

			}

		}



		/**
	     *
	     */
		public function deleteSpecificPrices()
		{

            $result = array();
            $query = new DbQuery();
            $query->select('a.date_start, a.date_end, sp.from, sp.to');
            $query->from('autoprice', 'a');
            $query->leftJoin('specific_price', 'sp', '(sp.from = a.date_start AND sp.to = a.date_end)');
            $query->where('id_autoprice = '.$this->id);
            $result = DB::getInstance()->executeS($query);

            foreach($result as $res){
	            DB::getInstance()->delete('specific_price', '`from` = "'.$res['date_start'].'" AND `to` = "'.$res['date_end'].'"');
	      	}

		}

	}

?>