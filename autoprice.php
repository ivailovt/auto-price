<?php

	if (!defined('_PS_VERSION_')) {
		    exit;
		}

	require dirname(__FILE__).'/inixframe/loader.php';
	


    /**
     * Class AutoPrice
     */
	class AutoPrice extends Inix2Module
	{

        private $reduction_taxes = array();

		public function __construct()
        {

            $this->name                   = 'autoprice';
            $this->tab                    = 'pricing_promotion';
            $this->version                = '1.0.0';
            $this->author                 = 'I.Stefanov';
            $this->need_instance 		  = 0;
            $this->ps_versions_compliancy = array('min' => '1.5.1.0', 'max' => '1.6.9.9');
            $this->bootstrap 			  = true;

            parent::__construct();

            $this->displayName = $this->l('Auto Price');
    		$this->description = $this->l('Create automated rules for the prices of certain products');

            if (!($this->context->controller instanceof AdminModulesController)) {
                $this->bootstrap = false;

                return;
            }

            $this->reduction_taxes = array(
                array('id' => 0, 'name' => $this->l('Tax excluded')),
                array('id' => 1, 'name' => $this->l('Tax included'))
            );

        }



        /**
         * @return string
         */
        public function getContent()
        {

            $this->_select = 'a.`id_autoprice`, b.`name`, a.`date_start`, a.`date_end`, a.`discount`, a.`active`, \'\'`category`';
            $this->context->controller->addCSS($this->getPathUri() . 'views/css/ap-admin.css', 'all');
            $this->className = 'AutoPrices';
            $this->object_table = 'autoprice';
            $this->object_identifier = 'id_autoprice';
            $this->_defaultOrderBy ='id_autoprice';
            $this->_defaultOrderWay = 'ASC';
            $this->show_toolbar = false;
            $this->bootstrap = true;
            $this->lang = true;

            $this->fields_list = array(
                'id_autoprice' => array(
                    'title' =>$this->l('ID'),
                    'type' => 'text',
                    'width' => 20,
                ),
                'name'  => array(
                    'title' => $this->l('Name rule'),
                    'type' => 'text',
                    'width' => 'auto'
                ),
                'currency' => array(
                    'title' =>$this->l('Currency'),
                    'type' => 'text',
                    'width' => 'auto',
                    'callback' => 'getCurrencyNames'
                ),
                'country' => array(
                    'title' =>$this->l('Country'),
                    'type' => 'text',
                    'width' => 'auto',
                    'callback' => 'getCountryNames'
                ),
                'group' => array(
                    'title' =>$this->l('Group'),
                    'type' => 'text',
                    'width' => 'auto',
                    'callback' => 'getGroupNames'
                ),
                'category'  => array(
                    'title' => $this->l('Category'),
                    'width' => 350,
                    'type' => 'text',
                    'callback' => 'getCategoryNames'
                ),
                'date_start' => array(
                    'title'  => $this->l('Period'),
                    //'type' => 'datetime',
                    'type' => 'text',
                    'width'  => 200,
                    'callback' => 'mergeDates'
                ),
                'quantity'  => array(
                    'title' => $this->l('Quantity'),
                    'type' => 'text',
                    'width' => 'auto',
                    'callback' => 'getQuantityValues'
                ),
                'discount'  => array(
                    'title' => $this->l('Discount'),
                    'type' => 'decimal',
                    'width' => 'auto',
                    'suffix' => '%'
                ),
                //'reduction_tax'  => array(
                    //'title' => $this->l('Reduction tax'),
                    //'type' => 'text',
                    //'width' => 'auto',
                    //'callback' => 'getTaxValues'
                //),
                'mode'  => array(
                    'title' => $this->l('Mode'),
                    'type' => 'text',
                    'width' => 'auto',
                    'callback' => 'getModeValues'
                ),
                'active'     => array(
                    'title'  => $this->l('Active'),
                    'active' => 'status',
                    'type'   => 'bool',
                    'ajax'   => false
                ),
            );

            $this->bulk_actions = array(
                'delete' => array(
                    'text' => $this->l('Delete selected'),
                    'confirm' => $this->l('Delete selected items?')
                ),
                
            );

            return parent::getContent();

        }



        /**
         * @return bool|string|void
         */
        public function renderList()
        {
       
            $this->addRowAction('edit');
            $this->addRowAction('delete');
            //$this->tpl_list_vars['title'] = $this->l('Fields');

            return parent::renderList();

        }



        /**
         * @param $id
         * @param $row
         *
         * @return string
         */
        public function getCurrencyNames($id, $row)
        {

            $currency = Currency::getCurrency($row['currency']);

            if($row['currency'] == 0) {
                return $this->l('All Currencies');
            }

            return $currency['name'];

        }



        /**
         * @param $id
         * @param $row
         *
         * @return string
         */
        public function getCountryNames($id, $row)
        {

            $countries = Country::getCountries((int)$this->context->language->id);

            if($row['country'] == 0) {
                return $this->l('All countries');
            }

            return $countries[$row['country']]['name'];

        }



        /**
         * @param $id
         * @param $row
         *
         * @return string
         */
        public function getGroupNames($id, $row)
        {

            $group_name = array();
            $groups = Group::getGroups((int)$this->context->language->id);

            foreach($groups as $group) {
                $group_name[$group['id_group']] = array($group['id_group'] => $group['name']);
            }

            if($row['group'] == 0) {
                return $this->l('All groups');
            }
          
            return $group_name[$row['group']][$row['group']];

        }



        /**
         * @param $id
         * @param $row
         *
         * @return string
         */
        public function getCategoryNames($id, $row)
        {

            $name = '';
            $names = '';
            $res = array();
            $query = new DbQuery();
            $query->select('cl.name');
            $query->from('category', 'c');
            $query->leftJoin('category_lang', 'cl', '(cl.id_category = c.id_category AND cl.id_lang = '.$this->context->language->id.')');
            $query->leftJoin('autoprice_categories', 'a', '(a.id_category = cl.id_category)');
            $query->where('id_autoprice = '.$row['id_autoprice']);
            $res = DB::getInstance()->executeS($query);

            foreach($res as $r) {
                $name .= $r['name'].', ';
                if($r['name'] != ''){
                    $names = substr($name, 0, -2);
                }
            }
           
            return $names;

        }



        /**
         * @param $id
         * @param $row
         *
         * @return string
         */
        public function mergeDates($id, $row)
        {
           
            return $this->l('From').' '.$row['date_start'].'<br>'.$this->l('to').' '.$row['date_end'];

        }



        /**
         * @param $id
         * @param $row
         *
         * @return string
         */
        public function getQuantityValues($id, $row)
        {

            return $row['quantity'];

        }



        /**
         * @param $id
         * @param $row
         *
         * @return string
         */
       // public function getTaxValues($id, $row)
       //{
 
            //return $this->reduction_taxes[$row['reduction_tax']]['name'];
      
        //}



        /**
         * @param $id
         * @param $row
         *
         * @return string
         */
        public function getModeValues($id, $row)
        {

            if($row['mode'] == 0) {
                return $this->l('Silent mode');
            }

            return $this->l('Active mode');

        }



        /**
         *
         */
        public function initContent()
        {

            $this->displayErrors();

            $this->getLanguages();
            // toolbar (save, cancel, new, ..)
            $this->initToolbar();
            $this->initPageHeaderToolbar();

            if ($this->display == 'feedback') {
                $this->context->controller->addJqueryPlugin('fancybox');
                $this->content .= $this->renderFeedback();

            } elseif ($this->display == 'bugreport') {
                $this->context->controller->addJqueryPlugin('fancybox');
                $this->content .= $this->renderBugReport();
            } elseif ($this->display == 'edit' || $this->display == 'add') {
                if ($this->has_configuration) {
                    if (!$this->loadObject(true)) {
                        return;
                    }
                    if ($this->className == 'AutoPrices') {
                        $this->content .= $this->renderForm();
                    }
                }
            } elseif ($this->display == 'view') {
                if ($this->has_configuration) {
                    if ($this->className) {
                        $this->loadObject(true);
                    }
                    $this->content .= $this->renderView();
                }
            } elseif ($this->display == 'details') {
                if ($this->has_configuration) {
                    $this->content .= $this->renderDetails();
                }
            } 
            elseif (!$this->ajax) {
                if ($this->has_configuration) {
                    if ($this->className == 'AutoPrices') {
                        $this->content .= $this->renderKpis();
                        $this->content .= $this->renderList();
                        $this->content .= $this->renderOptions();
                    }

                    // if we have to display the required fields form
                    if ($this->required_database) {
                        $this->content .= $this->displayRequiredFields();
                    }
                }
            }

            $this->context->smarty->assign(array(
                'content'                   => $this->content,
                'lite_display'              => $this->lite_display,
                'url_post'                  => self::$currentIndex . '&token=' . $this->token,
                'show_page_header_toolbar'  => $this->show_page_header_toolbar,
                'page_header_toolbar_title' => $this->page_header_toolbar_title,
                'title'                     => $this->page_header_toolbar_title,
                'toolbar_btn'               => $this->page_header_toolbar_btn,
                'page_header_toolbar_btn'   => $this->page_header_toolbar_btn
            ));

        }



        /**
         *
         */
        public function renderForm() 
        {

            $title = 'categories';
            $this->context->controller->addJS($this->getFramePathUri() . 'js/bootstrap-datetimepicker.js');
            $this->context->controller->addCSS($this->getFramePathUri() . 'css/datetimepicker.css');
            
            $this->show_form_cancel_button = true;
            $this->fields_form = array(
                'legend' => array(
                    'title' => Validate::isLoadedObject($this->object)? $this->l('Edit field'):  $this->l('Add field'),
                ),
                'input'  => array(
                    array(
                        'label'    => $this->l('Name rule'),
                        'type'     => 'text',
                        'name'     => 'name',
                        'lang'     => true,
                        'required' => true,
                        'class'    => 'md',
                        'col'      => 5
                    ),
                    array(
                         'label'            => $this->l('For'),
                         'required'         => false,
                         'type'             => 'select',
                         'name'             => 'currency',
                         'id'               => 'currency',
                         'form_group_class' => 'sm',
                            'options'       => array(
                                'query' => array_merge(array(0 => array('id_currency' => 0, 'name' => $this->l('All currencies'))), Currency::getCurrencies(false, true, true)),
                                'id'    => 'id_currency',
                                'name'  => 'name',
                            ),
                    ),
                    array(
                         //'label'            => $this->l('For'),
                         'required'         => false,
                         'type'             => 'select',
                         'name'             => 'country',
                         'id'               => 'country',
                         'form_group_class' => '',
                            'options'       => array(
                                'query' => array_merge(array(0 => array('id_country' => 0, 'name' => $this->l('All countries'))), Country::getCountries((int)$this->context->language->id)),
                                'id'    => 'id_country',
                                'name'  => 'name',
                            ),
                            'default' => array(
                                'label' => $this->l('No tax'),
                                'value' => 0
                            )
                    ), 
                     array(
                         //'label'            => $this->l('For'),
                         'required'         => false,
                         'type'             => 'select',
                         'name'             => 'group',
                         'id'               => 'group',
                         'form_group_class' => '',
                            'options'       => array(
                                'query' => array_merge(array(0 => array('id_group' => 0, 'name' => $this->l('All groups'))), Group::getGroups((int)$this->context->language->id)),
                                'id'    => 'id_group',
                                'name'  => 'name',
                            ),
                    ),
                     array(
                        'label'    => $this->l('Starting at'),
                        'type'     => 'text',
                        'name'     => 'quantity',
                        'required' => true,
                        'default_value' => 1,
                        'prefix'   => 'unit',
                        'class'    => 'sm',
                        'col'      => 3
                    ),
                    array(
                        'label'    => $this->l('Discount'),
                        'hint'     => $this->l('Discount product'),
                        'type'     => 'text',
                        'name'     => 'discount',
                        'default_value' => 0.00,
                        'lang'     => false,
                        'required' => true,
                        'prefix'   => '%',
                        'class'    => 'sm',
                        'col'      => 3
                    ),
                    //array(
                         //'label'            => $this->l('Apply a discount of'),
                         //'required'         => false,
                         //'type'             => 'select',
                         //'name'             => 'reduction_tax',
                         //'id'               => 'reduction_tax',
                         //'default_value'    => 1,
                         //'form_group_class' => '',
                            //'options'       => array(
                                //'query' => $this->reduction_taxes,
                                //'id'    => 'id',
                                //'name'  => 'name',
                            //),
                    //), 
                    array(
                        'type' => 'datetime',
                        'label' => $this->l('Date start'),
                        'name' => 'date_start',
                        'required' => true,
                        'hint' => $this->l('Datetime format: \'yyyy-mm-dd 00:00:00\''),
                        'class'=> 'datepicker hasDatepicker'
                    ),
                    array(
                        'type' => 'datetime',
                        'label' => $this->l('Date end'),
                        'name' => 'date_end',
                        'required' => true,
                        'hint' => $this->l('Datetime format: \'yyyy-mm-dd 00:00:00\''),
                        'class'=> 'datepicker hasDatepicker'
                    ),
                    array(
                        'label' => $this->l('Select categories to display'),
                        'type'  => 'categories',
                        'name'  => 'categories',
                        'required' => true,
                        'empty_message' => sprintf($this->l('Field %s is required.'), $title),
                        'tree'  => array(
                            'id'                  => 'categories-tree',
                            'selected_categories' => (Tools::getValue('categories', $this->object->categories)),
                            'disabled_categories' => array(),
                            'use_search'          => true,
                            'use_checkbox'        => true
                        ),
                        'is_array' => true,
                    ),
                    array(
                        'label'   => $this->l('Mode'),
                        'type'    => 'switch',
                        'name'    => 'mode',
                        'default_value' => 1,
                        'class'    => 't',
                        'required' => true,
                        'is_bool' => true,
                        'hint'     => $this->l('Switch to active mode in which to display the discount, or turn to silent mode, which does not show discount.'),
                        'values'  => array(
                            array(
                                'id'    => 'mode_on',
                                'value' => 1,
                                'label' => $this->l('Yes')
                            ),
                            array(
                                'id'    => 'mode_off',
                                'value' => 0,
                                'label' => $this->l('No')
                            )
                        ),
                    ),
                    array(
                        'label'   => $this->l('Active'),
                        'type'    => 'switch',
                        'name'    => 'active',
                        'default_value' => 1,
                        'class'    => 't',
                        'required' => false,
                        'is_bool' => true,
                        'values'  => array(
                            array(
                                'id'    => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Yes')
                            ),
                            array(
                                'id'    => 'active_off',
                                'value' => 0,
                                'label' => $this->l('No')
                            )
                        )
                    )
                ),
                'submit' => array(
                'title' => $this->l('  Save  '),
                //'class' => 'button',
                )
            );

            $this->fields_value = array(
                'discount' => number_format((($value = $this->getFieldValue($this->object, 'discount')) ? $value : 0), 2),
            );

            return parent::renderForm();

        }



        /**
         *
         */
        public function deleteAllSpecificPrices()
        {

            $result = array();
            $query = new DbQuery();
            $query->select('a.date_start, a.date_end, sp.from, sp.to');
            $query->from('autoprice', 'a');
            $query->leftJoin('specific_price', 'sp', '(sp.from = a.date_start AND sp.to = a.date_end)');
            $result = DB::getInstance()->executeS($query);

            foreach($result as $res){
                DB::getInstance()->delete('specific_price', '`from` = "'.$res['from'].'" AND `to` = "'.$res['to'].'"');
            }

        }



        /**
         *
         */
        public function displayErrors()
        {

            if (Tools::isSubmit('submitAddautoprice')) {
                $error = new AutoPrices();
                $error->categories = Tools::getValue('categories');
                $error->date_start = Tools::getValue('date_start');
                $error->date_end = Tools::getValue('date_end');
                $error->discount = Tools::getValue('discount');
                $errors = $error->dateValidate();
  
                if($errors != '') {
                    foreach($errors as $e){
                        $this->errors[] = $this->l($e);
                    }
                }
            }

        }



        /**
         * @return bool
         */
        public function uninstall()
        {

            $this->deleteAllSpecificPrices();
            $uninstall = parent::uninstall();

            return $uninstall;

        }

	}

?>