CREATE TABLE IF NOT EXISTS `PREFIX_autoprice` (
  `id_autoprice` int(11) NOT NULL AUTO_INCREMENT,
  `currency` int(11) NOT NULL,
  `country` int(11) NOT NULL,
  `group` int(11) NOT NULL,
  `date_start` datetime NOT NULL,
  `date_end` datetime NOT NULL,
  `quantity` int(11) NOT NULL,
  `discount` decimal(20,6) NOT NULL,
  /*`reduction_tax` int(11) NOT NULL,*/
  `mode` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_autoprice`)
) ENGINE=_ENGINE_ DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `PREFIX_autoprice_lang` (
  `id_autoprice` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`id_autoprice`,`id_lang`)
) ENGINE=_ENGINE_ DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `PREFIX_autoprice_categories` (
  `id_autoprice` int(11) NOT NULL,
  `id_category` int(11) NOT NULL,
  PRIMARY KEY (`id_autoprice`,`id_category`)
)  DEFAULT CHARSET=utf8;
